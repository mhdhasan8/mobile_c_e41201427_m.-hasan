import 'dart:html';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Minggu4',
      home: Scaffold(
        backgroundColor: Colors.lightBlue,
        appBar: AppBar(
          title: Text('Pertemuan Minggu 4'),
          backgroundColor: Colors.blueAccent,
        ),
        body: Column(
          children: <Widget>[
            Image.asset('assets/images/contoh.jpg'),
            Text(
              'Diatas adalah foto Monyet tidur',
              style: TextStyle(fontSize: 24, fontFamily: "Serif", height: 2.0),
            ),
            Text('Dan dibawah ini adalah foto monyet tersenyum:)'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.network(
                  "https://i.pinimg.com/564x/f3/54/03/f35403a38cc11495218cd04cdc4cec58.jpg",
                  width: 220.0,
                  height: 220.0,
                ),
                Image.network(
                  "https://i.pinimg.com/564x/4d/c7/e7/4dc7e70b74691b023fed360c9fa23eb5.jpg",
                  width: 220.0,
                  height: 220.0,
                ),
              ],
            ),
            Text('Klik disini untuk melihat hewan lain'),
            Column(
              children: <Widget>[Icon(Icons.access_alarm), Text('Alarm')],
            )
          ],
        ),
      ),
    );
  }
}
