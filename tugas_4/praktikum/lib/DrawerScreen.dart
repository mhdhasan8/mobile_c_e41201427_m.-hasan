import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget> [
          UserAccountsDrawerHeader(
            accountName: Text("M. Hasan"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/v.jpg")
            ),
            accountEmail: Text("hasanmuhammad197@gmail.com"),
          ),
        ],
      ),
    )
  }
}
