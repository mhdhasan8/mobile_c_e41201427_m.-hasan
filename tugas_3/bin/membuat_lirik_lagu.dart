import 'dart:async';

void main(List<String> args) {
  print("Lagu siap dimulai");
  print("=================");

  var time1 = Timer(
      Duration(seconds: 1), () => print("Ku melangkah pasti menyongsong hari"));
  var time2 = Timer(
      Duration(seconds: 3), () => print("Tegar aku berdiri berbekal ilmu"));
  var time3 = Timer(
      Duration(seconds: 5), () => print("Membangun negeri putra pertiwi"));
  var time4 =
      Timer(Duration(seconds: 8), () => print("Bersatu bersamamu Almamaterku"));
  var time5 = Timer(
      Duration(seconds: 10),
      () =>
          print("Gerak.... Semangat.... Wujudkan karya terlaksana tri dharma"));

  Future.delayed(Duration(seconds: 11), () => print(' '));
  Future.delayed(
      Duration(seconds: 13), () => print('Menggenggam asa meraih cita-cita'));
  Future.delayed(
      Duration(seconds: 16), () => print('di Politeknik Negeri Jember'));
  Future.delayed(
      Duration(seconds: 19), () => print('Megah kau berdiri membangun negeri'));
  Future.delayed(
      Duration(seconds: 21), () => print('Bersatu bersamamu tumbuh cintamu'));
  Future.delayed(
      Duration(seconds: 23), () => print('Berjajarlah kamu anak negeri'));
  Future.delayed(
      Duration(seconds: 25), () => print('Indah engkau dihati, Almamaterku'));
}
