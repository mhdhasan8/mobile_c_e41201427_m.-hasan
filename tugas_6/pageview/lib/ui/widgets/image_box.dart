import 'package:division/division.dart';
import 'package:flutter/material.dart';
import 'package:pageview/ui/styles/styles.dart';

class ImageBox extends StatelessWidget {
  final String url;

  ImageBox(this.url);

  @override
  Widget build(BuildContext context) {
    return Parent(
      style: parentStyle.clone()..background.image(url: url, fit: BoxFit.cover),
    );
  }
}
