import 'package:flutter/material.dart';
import 'package:pageview/ui/widgets/image_box.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PageController controller =
        PageController(initialPage: 0, viewportFraction: 0.6);
    List<String> urls = [
      "https://images-na.ssl-images-amazon.com/images/I/A1t8xCe9jwL._SL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/A1hFzTczzJL._SL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/91wc7yc2R8L._SL1500_.jpg",
      "https://images-na.ssl-images-amazon.com/images/I/A1t8xCe9jwL._SL1500_.jpg"
    ];
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text("PageView Hasan"),
        ),
        body: PageView.builder(
          controller: controller,
          itemCount: urls.length,
          itemBuilder: (context, index) => Center(
            child: ImageBox(urls[index]),
          ),
        ));
  }
}
