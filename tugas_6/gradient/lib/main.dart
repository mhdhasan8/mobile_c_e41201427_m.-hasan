import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Praktikum Gradient',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Flexible(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[
                      Colors.cyan,
                      Colors.yellowAccent,
                    ])),
              )),
          Flexible(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                    gradient: RadialGradient(radius: 2.0, colors: <Color>[
                  Colors.greenAccent,
                  Colors.blueAccent,
                  Colors.yellowAccent,
                ])),
              )),
          Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                    gradient: SweepGradient(startAngle: 1.5, colors: <Color>[
                  Colors.blue.shade200,
                  Colors.greenAccent,
                  Colors.blueAccent,
                ])),
              ))
        ],
      ),
    );
  }
}
