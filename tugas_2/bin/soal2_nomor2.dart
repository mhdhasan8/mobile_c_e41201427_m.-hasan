import 'dart:io';

void main(List<String> args) {
  print("Masukkan nama anda : ");

  var nama = stdin.readLineSync();

  if (nama == "") {
    print("Nama harus diisi!");
  } else {
    print(
        "Pilih peran anda untuk memulai game dengan menulis angka saja ! \n1. Penyihir \n2. Guard \n3. Werewolf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Halo $nama, Pilih peranmu untuk memulai game!");
    } else if (peran == "1") {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan.");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Werewolf, $nama");
      print("Halo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Peran tidak ditemukan!");
    }
  }
}
